# OpenML dataset: mfeat-morphological

https://www.openml.org/d/18

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Robert P.W. Duin, Department of Applied Physics, Delft University of Technology  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Multiple+Features) - 1998  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)   

**Multiple Features Dataset: Morphological**  
One of a set of 6 datasets describing features of handwritten numerals (0 - 9) extracted from a collection of Dutch utility maps. Corresponding patterns in different datasets correspond to the same original character. 200 instances per class (for a total of 2,000 instances) have been digitized in binary images. 

In this dataset, these digits are represented in terms of 6 morphological features. 

### Attribute Information  
The meaning of the features is mostly unknown. They are never named in the original files, and the paper only talks about 'morphological features, such as the number of endpoints'.

### Relevant Papers  
A slightly different version of the database is used in  
M. van Breukelen, R.P.W. Duin, D.M.J. Tax, and J.E. den Hartog, Handwritten digit recognition by combined classifiers, Kybernetika, vol. 34, no. 4, 1998, 381-386.
 
The database as is is used in:  
A.K. Jain, R.P.W. Duin, J. Mao, Statistical Pattern Recognition: A Review, IEEE Transactions on Pattern Analysis and Machine Intelligence archive, Volume 22 Issue 1, January 2000

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/18) of an [OpenML dataset](https://www.openml.org/d/18). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/18/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/18/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/18/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

